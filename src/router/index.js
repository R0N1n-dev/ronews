import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Tech from "../views/Tech.vue";
import Health from "../views/Health.vue";
import About from "../views/About.vue";

Vue.use(VueRouter);
const routes = [{
        path: "/",
        name: "Home",
        component: Home,
    },
    {
        path: "/tech",
        name: "Tech",
        component: Tech,
    },
    {
        path: "/health",
        name: "Health",
        component: Health,
    },
    {
        path: "/about",
        name: "About",
        component: About,
    },
];

const router = new VueRouter({ routes, mode: "history" });

export default router;