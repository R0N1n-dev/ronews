import Vue from "vue";
import "virtual:windi.css";
import Vuesax from "vuesax";
import "vuesax/dist/vuesax.css";
import router from './router';
import App from "./App.vue";
Vue.use(Vuesax);

new Vue({
    router,
    render: (h) => h(App),
}).$mount("#app");