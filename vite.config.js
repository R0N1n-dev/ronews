import WindiCss from "vite-plugin-windicss";
import { createVuePlugin } from "vite-plugin-vue2";
import ViteComponents from "vite-plugin-components";

module.exports = {
    plugins: [createVuePlugin(), WindiCss(), ViteComponents()],
};